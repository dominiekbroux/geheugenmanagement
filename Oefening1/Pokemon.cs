﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geheugenmanagement.Oefening1
{
    class Pokemon
    {

		public Pokemon(int inputHP, int inputMaxHP)
		{
			hp = inputHP;
			maxHP = inputMaxHP;
		}

		private int maxHP;
		public int MaxHP
		{
			get { return maxHP; }
			set
			{
				maxHP = value;
				if (maxHP < 20) maxHP = 20;
				if (maxHP > 1000) maxHP = 1000;
			}
		}

		private int hp;
		public int HP
		{
			get { return hp; }
			set
			{
				hp = value;
				if (hp > maxHP) hp = maxHP;
				if (hp < 0) hp = 0;
			}
		}
		public PokeSpecies PokeSpecies { get; set; }
		public PokeTypes PokeTypes { get; set; }
		public void Attack()
		{
			switch (this.PokeTypes)	
			{
				case PokeTypes.Grass:
					Console.ForegroundColor = ConsoleColor.Green;
					break;
				case PokeTypes.Fire:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case PokeTypes.Water:
					Console.ForegroundColor = ConsoleColor.Blue;
					break;
				case PokeTypes.Electric:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				default:
					break;
			}
			Console.WriteLine($"{this.PokeSpecies}!".ToUpper());
			
		}

		public static void MakePokemon()
		{
			var pokemon = new List<Pokemon>();

			for (int i = 0; i < Enum.GetNames(typeof(PokeSpecies)).Length; i++)
			{
				pokemon.Add(new Pokemon(20,20));
				pokemon[i].PokeSpecies = (PokeSpecies)i;
				pokemon[i].PokeTypes = (PokeTypes)i;
				pokemon[i].Attack();
			}
			Console.ReadLine();
		}
	}
}
